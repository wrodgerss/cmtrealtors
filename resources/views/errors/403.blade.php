@extends('layouts.app')

@section('content')
    <h2 class="page-title">Not authorized to view this page</h2>
    <a onclick="javascript:window.history.back()">Go back</a>
@endsection
