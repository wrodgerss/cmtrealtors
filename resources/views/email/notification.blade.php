<h3>Hello</h3>

<h5>
    The following instruction has been marked as delivered: <br>
    <small>
        {{ $instruction->title_no }}
    </small>
</h5>

<hr>

<h5>
    <small>Delivered on: {{ $instruction->delivery_date }}</small>
    <br>
    <small>Status: {{ $instruction->status }}</small>
    <br>
    <small>Branch: {{ $instruction->branch->name }}</small>
</h5>

<a href="{{ route('login') }}">SIGN IN</a>

<hr>

<p>&copy; - {{ date('Y') }} CMT-REALTORS. All rights reserved</p>
