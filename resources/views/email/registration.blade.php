<h3>Hello {{ $user->name }}</h3>

<h5>
    You are registered as a user of CMT-REALTORS @if($user->role != 'Director' && $user->role != 'Administrator') {{  $user->branch->name  }} branch. @endif <br>
    <small>
        Click the button below to access your account
    </small>
</h5>

<hr>

<h5>
    Login credentials <br>
    <small>email: {{ $user->email }}</small> <br>
    <small>
        password: @if($user->role == 'Director') sys-admin@cmtrealtors @elseif($user->role == 'Administrator') sys-admin@cmtrealtors @else {{  'cmtrealtors@' . $user->branch->name  }} @endif
    </small>
</h5>

<a href="{{ route('login') }}">SIGN IN</a>

<hr>

<p>&copy; - {{ date('Y') }} CMT-REALTORS. All rights reserved</p>
