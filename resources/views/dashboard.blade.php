<!doctype html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>CMT Realtors | @yield('title')</title>
        <!-- jquery -->
        <script src="{{ Asset('vendor/jquery.js') }}"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" href="{{ Asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <script src="{{ Asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- font-awesome -->
        <link rel="stylesheet" href="{{ Asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <!-- css -->
        <link rel="stylesheet" href="{{ asset('css/dashboard/style.css') }}">
        @yield('css')
    </head>

    <body>
        <nav class="navbar">
            <div class="container-fluid">
                <div class="navbar-header">
                    <a href="" class="navbar-brand">
                        <span>cmt</span>&nbsp;<sup>realtors</sup>
                    </a>
                </div>
                <ul class="nav navbar-nav navbar-right">
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-user"></i>
                            {{ Auth::user()->name }} 
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu" role="menu">
                            <li>
                                <a href="{{ route('logout') }}"
                                    onclick="event.preventDefault();
                                            document.getElementById('logout-form').submit();">
                                    Logout
                                </a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                </form>
                            </li>
                        </ul>
                    </li>
                </ul>
            </div>
        </nav>
        <div id="aside-menu">
            <ul>
                <li>
                    <a href="{{ route('instructions.create') }}" class="btn clearfix" id="new-instruction">
                        <span class="pull-left">
                            new instruction
                        </span>
                        <span class="pull-right">
                            <i class="fa fa-plus"></i>
                        </span>
                    </a>
                </li>
                @if(Auth::user()->isAdmin)
                    <li>
                        <a href="{{ route('members.index') }}">
                            <i class="fa fa-user"></i>
                            members
                        </a>
                        <a href="{{ route('members.create') }}" class="pull-right add">
                            add
                        </a>
                    </li>
                    <li>
                        <a href="{{ route('branches.index') }}">
                            <i class="fa fa-building"></i>
                            branches
                        </a>
                        <a href="{{ route('branches.create') }}" class="pull-right add">
                            add
                        </a>
                    </li>
                @endif
                <li>
                    <a href="{{ route('instructions.index') }}">
                        <i class="fa fa-briefcase"></i>
                        instructions
                    </a>
                </li>
                <li>
                    <a href="{{ route('settings') }}">
                        <i class="fa fa-cog"></i>
                        settings
                    </a>
                </li>
                <li>
                    <a href="">
                        <i class="fa fa-home"></i>
                        home
                    </a>
                </li>
                <li class="divider"></li>
                <li>
                    <a href="{{ route('feedback') }}">
                        feedback
                    </a>
                </li>
            </ul>
            <div id="briwan">
                <a href="">
                    powered by BRIWAN
                </a>
            </div>
            <div id="footer">
                &copy; - {{ date('Y') }}. cmt realtors
            </div>
        </div>
        <div id="content">
            @yield('content')
        </div>
        @yield('js')
    </body>

</html>
