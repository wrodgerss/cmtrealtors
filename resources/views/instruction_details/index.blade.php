@extends('dashboard')

@section('title')
    Instruction details list
@endsection

@section('css')
    <link rel="stylesheet" href="{{ Asset('css\instruction_details\index.css') }}">
@endsection

@section('content')
    <form action="{{ route('instrunctions.search') }}" class="form-inline" id="search-form">
        <label class="text-uppercase">
            filter by:
        </label>
        <div class="input-group">
            <select name="filter" class="form-control" required id="filter">
                <option value="status">status</option>
                <option value="date">date</option>
                @if(Auth::user()->isAdmin)
                    <option value="staff">staff</option>
                    <option value="branch">branch</option>
                @endif
            </select>
        </div>
        <div class="input-group">
            <input type="search" class="form-control" placeholder="Type search query" required name="query" id="query">
        </div>
        <button type="submit" class="btn btn-success text-uppercase">
            search
        </button>
    </form>
    <table>
        <tr id="table-head">
            <th>status</th>
            <th>title no</th>
            <th>posted</th>
            <th>track time</th>
        </tr>
        @foreach($instructions as $instruction)
            <tr>
                <td id="status">
                    @if($instruction->status == 'pending')
                        <i class="fa fa-calendar-plus-o pending"></i>
                        pending
                    @elseif($instruction->status == 'completed')
                        <i class="fa fa-calendar-check-o completed"></i>
                        completed:
                        <span>
                            {{ $instruction->delivery_date->format('j F Y') }}
                        </span>
                    @else
                        <i class="fa fa-calendar-times-o expired"></i>
                        expired:
                        @if( $instruction->delivery_date != null )
                            {{ $instruction->delivery_date->format('j F Y') }}
                        @else
                            not delivered
                        @endif
                    @endif
                </td>
                <td id="desc">
                    {{ $instruction->short_desc() }}
                    <span>-</span>
                </td>
                <td>
                    {{ $instruction->created_at->format('j F Y') }}
                </td>
                <td>
                    {{ $instruction->track_time->format('j F Y') }}
                </td>
                <td>
                    <div class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                            <i class="fa fa-ellipsis-h"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right" role="menu">
                            <li>
                                <a href="{{ route('instructions.show', $instruction->id) }}">
                                    <i class="fa fa-eye"></i>
                                    &nbsp; view
                                </a>
                            </li>
                        </ul>
                    </div>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $instructions->links() }}
@endsection

@section('js')
    <script src="{{ asset('js/search-form.js') }}"></script>
@endsection
