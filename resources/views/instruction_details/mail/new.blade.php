<h3>Hello</h3>

<h5>
    The following are the new instruction details: <br>
    <small>
        {{ ucfirst($instruction->title_no) }}
    </small>
</h5>

<hr>

<h5>
    <small>Bank: {{ ucwords($instruction->bank) }}</small>
    <br>
    <small>Bank branch: {{ ucwords($instruction->bank_branch) }}</small>
    <br>
    <small>Client contact: {{ $instruction->client_contacts }}</small>
    <br>
    <small>Track time: {{ $instruction->track_time->format('j F Y') }}</small>
    <br>
    <small>Expenditure: {{ number_format($instruction->expenditure) }}</small>
    <br>
</h5>

<hr>

<h5>
    <small>Staff: {{ ucwords($instruction->user->name) }}</small>
    <br>
    <small>Branch office: {{ ucwords($instruction->branch->name) }}</small>
    <br>
</h5>

<a href="{{ route('instructions.show', $instruction->id) }}">
    VIEW INSTRUCTION
</a>

<hr>

<p>&copy; - {{ date('Y') }} CMT-REALTORS. All rights reserved</p>
