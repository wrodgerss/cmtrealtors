@extends('dashboard')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/instruction_details/show.css') }}">
@endsection

@section('title')
    {{ $instruction->short_desc() }}
@endsection

@section('content')
    <h4 id="status">
        current status:
        @if($instruction->status == 'pending')
            <i class="fa fa-calendar-plus-o pending"></i>
            pending
        @elseif($instruction->status == 'completed')
            <i class="fa fa-calendar-check-o completed"></i>
            completed
        @else
            <i class="fa fa-calendar-times-o expired"></i>
            expired
        @endif
        @can('update', $instruction)
            <small class="pull-right">
                <a href="{{ route('instructions.edit', $instruction->id) }}" class="btn btn-link">
                    <i class="fa fa-pencil-square-o"></i>
                    edit
                </a>
            </small>
        @endcan
        @can('delete', $instruction)
            <small class="pull-right">
                <form action="{{ route('instructions.destroy', $instruction->id) }}" method="post" class="pull-right">
                    {{ method_field('DELETE') }}
                    {{ csrf_field() }}
                    <button onclick="javascript:return confirm('Are you sure to delete this item?')" type="submit" class="btn btn-link">
                        <i class="fa fa-trash"></i>
                        delete
                    </button>
                </form>
            </small>
        @endcan
    </h3>
    <div class="media">
        <div class="media-left media-middle">
            <i class="fa fa-building media-object" aria-hidden="true"></i>
        </div>
        <div class="media-body">
             <h2>
                branch
                <small>
                    < {{ $instruction->branch->name }} >
                </small>
                @if($instruction->delivery_date == null)
                    @can('update', $instruction)
                        <form action="{{ route('instruction.delivered', ['instruction' => $instruction->id]) }}" method="post" class="pull-right" id="deliver">
                            {{ method_field('PATCH') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn">
                                mark as delivered
                            </button>
                        </form>
                    @endcan
                @else
                    <small class="pull-right" id="delivered">
                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                        &nbsp; delivered on {{ $instruction->delivery_date->format('j F Y') }}
                    </small>      
                @endif
             </h2>
             <h6>
                posted on {{ $instruction->created_at->format('j F Y') }}
            </h6>
            <h6 id="track_time">
                track time <span>{{ $instruction->track_time->format('j F Y') }}</span>
            </h6>
        </div>
    </div>
    <p id="title_no">
        {{ $instruction->title_no }}
    </p>
    <h5>
        <b>bank:</b> 
        {{ $instruction->bank }} - {{ $instruction->bank_branch }}
    </h5>
    <h5>
        <b>client contacts:</b>
        {{ $instruction->client_contacts }}
    </h5>
    @if( $instruction->delivery_date != null )
        <h5>
            <b>client invoice:</b>
            {{ $instruction->payment }}
        </h5>
    @endif
    <h5>
        <b>expenditure:</b>
        KES. {{ number_format($instruction->expenditure) }}
    </h5>
    @if(Auth::user()->isAdmin)
        <h5>
            <b>assigned to:</b>
            <span class="text-capitalize">
                {{ $instruction->user->name }}
            </span>
        </h5>
    @endif
    <h5>

    </h5>
@endsection
