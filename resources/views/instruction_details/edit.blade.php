@extends('dashboard')

@section('title')
    Edit instruction
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/forms/style.css') }}">
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <i class="fa fa-briefcase media-object"></i>
        </div>
        <div class="media-body">
            <h4 class="media-heading">
                Edit instruction
            </h4>
            <form action="{{ route('instructions.update', $instruction) }}" class="form-horizontal" method="post">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="bank" class="control-label col-md-3 col-sm-3">
                        Bank:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="bank" name="bank" value="{{ $instruction->bank }}" required autofocus placeholder="Enter name of the bank">
                    </div>
                    @if($errors->has('bank'))
                        <span class="help-block">
                            <strong>{{ $errors->first('bank') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="bank_branch" class="control-label col-md-3 col-sm-3">
                        Branch:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="bank_branch" name="bank_branch" value="{{ $instruction->bank_branch }}"  required placeholder="Enter bank branch">
                    </div>
                    @if($errors->has('bank_branch'))
                        <span class="help-block">
                            <strong>{{ $errors->first('bank_branch') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="client_contacts" class="control-label col-md-3 col-sm-3">
                        Client contacts:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="tel" class="form-control" id="client_contacts" name="client_contacts" value="{{ $instruction->client_contacts }}" required placeholder="Enter client mobile number">
                    </div>
                    @if($errors->has('client_contacts'))
                        <span class="help-block">
                            <strong>{{ $errors->first('client_contacts') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="track_time" class="control-label col-md-3 col-sm-3">
                        Track time:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="date" class="form-control" id="track_time" name="track_time" value="{{ $instruction->track_time->format('Y-m-d') }}" required>
                    </div>
                    @if($errors->has('track_time'))
                        <span class="help-block">
                            <strong>{{ $errors->first('track_time') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="title_no" class="control-label col-md-3 col-sm-3">
                        Title no:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="title_no" name="title_no" value="{{ $instruction->title_no }}" required placeholder="Enter instruction title details">
                    </div>
                    @if($errors->has('title_no'))
                        <span class="help-block">
                            <strong>{{ $errors->first('title_no') }}</strong>
                        </span>
                    @endif
                </div>
                @if(Auth::user()->isAdmin)
                     <div class="form-group">
                        <label for="user_id" class="control-label col-md-3 col-sm-3">
                            Staff:
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <select name="user_id" id="staff">
                                @foreach(App\User::where('isAdmin', '!=', '1')->get() as $user)
                                    <option value="{{ $user->id }}" {{ ($instruction->user_id == $user->id ? 'selected' : '') }}>
                                        {{ $user->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        @if($errors->has('user_id'))
                            <span class="help-block">
                                <strong>{{ $errors->first('user_id') }}</strong>
                            </span>
                        @endif
                    </div>
                @endif
                <div class="form-group">
                    <label for="expenditure" class="control-label col-md-3 col-sm-3">
                        Expenditure:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="expenditure" name="expenditure" value="{{ $instruction->expenditure }}" required placeholder="Enter money requisition">
                    </div>
                    @if($errors->has('expenditure'))
                        <span class="help-block">
                            <strong>{{ $errors->first('expenditure') }}</strong>
                        </span>
                    @endif
                </div>
                @if( $instruction->delivery_date != null )
                    <div class="form-group">
                        <label for="payment" class="control-label col-md-3 col-sm-3">
                            Payment:
                        </label>
                        <div class="col-md-6 col-sm-6">
                            <select name="payment" id="payment">
                                <option value="not paid" @if($instruction->payment == 'not paid') selected @endif>
                                    Not paid
                                </option>
                                <option value="paid" @if($instruction->payment == 'paid') selected @endif>
                                    Paid
                                </option>
                            </select>
                        </div>
                        @if($errors->has('payment'))
                            <span class="help-block">
                                <strong>{{ $errors->first('payment') }}</strong>
                            </span>
                        @endif
                    </div>
                @endif
                <button class="btn form-control" type="submit">
                    submit
                </button>
            </form>
        </div>
    </div>
@endsection
