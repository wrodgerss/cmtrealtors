@extends('master')

@section('title')
    Login
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/auth/style.css') }}">
@endsection

@section('content')
    @include('auth.navbar')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <form role="form" method="POST" action="{{ route('login') }}">
                    {{ csrf_field() }}
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon">    
                                <i class="fa fa-envelope-o"></i>
                            </span>
                            <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Your email">
                        </div>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        <div class="input-group">
                            <span class="input-group-addon">
                                <i class="fa fa-lock"></i>
                            </span>
                            <input id="password" type="password" class="form-control" name="password" required placeholder="Enter password">
                            <span class="input-group-addon">
                                <a href="{{ route('password.request') }}">
                                    Forgot?
                                </a>
                            </span>
                        </div>
                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="checkbox">
                        <label>
                            <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : '' }}> Remember Me
                        </label>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn form-control">
                            Login
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
