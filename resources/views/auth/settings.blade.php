@extends('dashboard')

@section('title')
    settings
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/forms/style.css') }}">
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <i class="fa fa-user media-object"></i>
        </div>
        <div class="media-body">
            <h4 class="media-heading">
                Account setup
            </h4>
            <form action="{{ route('change_email') }}" class="form-horizontal" method="post">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="email" class="control-label col-md-3 col-sm-3">
                        Email:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="email" class="form-control" id="email" name="email" required autofocus value="{{ Auth::user()->email }}">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <button type="submit" class="btn">
                            change
                        </button>
                    </div>
                    @if($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
            </form>
            @if(session('success_email'))
                <p class="alert-success">
                    {{ session()->get('success_email') }}
                </p>
            @endif
            <form action="{{ route('change_password') }}" class="form-horizontal" method="post">
                {{ method_field('PATCH') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="password" class="control-label col-md-3 col-sm-3">
                        Password:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="password" class="form-control" id="password" name="password" required placeholder="*********">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <button type="submit" class="btn">
                            change
                        </button>
                    </div>
                    @if($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                    @endif
                </div>
            </form>
            @if(session('success_password'))
                <p class="alert-success">
                    {{ session()->get('success_password') }}
                </p>
            @endif
        </div>
    </div>
@endsection