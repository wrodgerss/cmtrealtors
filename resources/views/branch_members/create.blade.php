@extends('dashboard')

@section('title')
    create branch member
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/forms/style.css') }}">
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <i class="fa fa-user-plus media-object"></i>
        </div>
        <div class="media-body">
            <h4 class="media-heading">
                Create branch member
            </h4>
            <form action="{{ route('members.store') }}" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="control-label col-md-3 col-sm-3">
                        Name:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus placeholder="Name">
                    </div>
                    @if($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="email" class="control-label col-md-3 col-sm-3">
                        Email:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required placeholder="Email address">
                    </div>
                    @if($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="phone" class="control-label col-md-3 col-sm-3">
                        Contact:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required placeholder="Phone number">
                    </div>
                    @if($errors->has('phone'))
                        <span class="help-block">
                            <strong>{{ $errors->first('phone') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="branch" class="control-label col-md-3 col-sm-3">
                        Branch:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <select name="branch" id="branch">
                            @foreach($branches as $branch)
                                <option value="{{ $branch->id }}">
                                    {{ $branch->name }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                    @if($errors->has('branch'))
                        <span class="help-block">
                            <strong>{{ $errors->first('branch') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="role" class="control-label col-md-3 col-sm-3">
                        Position:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input id="role" type="text" class="form-control" name="role" value="{{ old('role') }}" required placeholder="Role at the branch">
                    </div>
                    @if($errors->has('role'))
                        <span class="help-block">
                            <strong>{{ $errors->first('role') }}</strong>
                        </span>
                    @endif
                </div>
                <button class="btn form-control" type="submit">
                    register
                </button>
            </form>
        </div>
    </div>
@endsection
