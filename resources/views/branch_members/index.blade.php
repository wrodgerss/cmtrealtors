@extends('dashboard')

@section('title')
    Instruction details list
@endsection

@section('css')
    <link rel="stylesheet" href="{{ Asset('css\branch_members\index.css') }}">
@endsection

@section('content')
    <table style="text-transform: capitalize">
        <tr id="table-head">
            <th>name</th>
            <th>email</th>
            <th>phone</th>
            <th>branch</th>
            <th>position</th>
        </tr>
        @foreach($members as $member)
            <tr>
                <td>
                    {{ $loop->iteration }}. {{ ucwords($member->name) }}
                </td>
                <td id="email">
                    {{ $member->email }}
                    <span>-</span>
                </td>
                <td>
                    {{ $member->phone }}
                </td>
                <td>
                    {{ $member->branch->name }}
                </td>
                <td>
                    {{ $member->role }}
                </td>
                <td>
                    <form action="{{ route('members.destroy', $member->id) }}" method="post">
                        {{ method_field('DELETE') }}
                        {{ csrf_field() }}
                        <button onclick="javascript:return confirm('Are you sure to delete this member')" id="delete" type="submit">
                            <i class="fa fa-trash"></i>
                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>
    {{ $members->links() }}
@endsection
