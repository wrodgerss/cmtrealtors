@extends('dashboard')

@section('title', 'branches')

@section('css')
    <link rel="stylesheet" href="{{ asset('css/branches/index.css') }}">
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <i class="fa fa-building media-object"></i>
        </div>
        <div class="media-body">
            <h4 class="media-heading">
                branches
            </h4>
            <table>
                @foreach($branches as $branch)
                    <tr>
                        <td>
                            {{ $loop->iteration }}. {{ $branch->name }}
                        </td>
                        <td>
                            <a href="{{ route('branches.edit', $branch->id) }}">
                                edit
                            </a>
                            &nbsp; | &nbsp;
                            <form action="{{ route('branches.destroy', $branch->id) }}" method="post">
                                {{ method_field('DELETE') }}
                                {{ csrf_field() }}
                                <button type="submit" onclick="javascript:return confirm('Are you surer to delete this branch')" id="delete">
                                    <i class="fa fa-trash"></i>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>
@endsection
