@extends('dashboard')

@section('title')
    edit {{ $branch->name }} branch
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/forms/style.css') }}">
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <i class="fa fa-building media-object"></i>
        </div>
        <div class="media-body">
            <h4 class="media-heading">
                Edit branch
            </h4>
            <form action="{{ route('branches.update', $branch->id) }}" class="form-horizontal" method="post">
                {{ method_field('PUT') }}
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="name" class="control-label col-md-3 col-sm-3">
                        Name:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="name" name="name" required autofocus value="{{ $branch->name }}" placeholder="Enter name of the branch">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <button type="submit" class="btn">
                            save
                        </button>
                    </div>
                    @if($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
            </form>
            @if(session('success'))
                <p class="alert-success">
                    {{ session()->get('success') }}
                </p>
            @endif
        </div>
    </div>
@endsection
