@extends('dashboard')

@section('title')
    settings
@endsection

@section('css')
    <link rel="stylesheet" href="{{ asset('css/forms/style.css') }}">
@endsection

@section('content')
    <div class="media">
        <div class="media-left">
            <i class="fa fa-envelope media-object"></i>
        </div>
        <div class="media-body">
            <h4 class="media-heading">
                Send message about any issue
            </h4>
            <form action="{{ route('send_message') }}" class="form-horizontal" method="post">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="message" class="control-label col-md-3 col-sm-3">
                        Feedback:
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" class="form-control" id="message" name="message" required autofocus placeholder="Enter your message">
                    </div>
                    <div class="col-md-3 col-sm-3">
                        <button type="submit" class="btn">
                            send
                        </button>
                    </div>
                </div>
            </form>
            @if(session('success'))
                <p class="alert-success">{{ session()->get('success') }}</p>
            @endif
        </div>
    </div>
@endsection