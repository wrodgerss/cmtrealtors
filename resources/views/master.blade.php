<!doctype html>

<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>CMT Realtors | @yield('title')</title>
        <!-- jquery -->
        <script src="{{ Asset('vendor/jquery.js') }}"></script>
        <!-- bootstrap -->
        <link rel="stylesheet" href="{{ Asset('vendor/bootstrap/css/bootstrap.min.css') }}">
        <script src="{{ Asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
        <!-- font-awesome -->
        <link rel="stylesheet" href="{{ Asset('vendor/font-awesome/css/font-awesome.min.css') }}">
        <!-- css -->
        @yield('css')
    </head>

    <body>
        @yield('content')
    </body>

</html>
