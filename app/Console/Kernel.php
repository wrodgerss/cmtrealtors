<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\InstructionDetail;
use App\User;
use Mail;
use App\Mail\ExpiredInstruction;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $instructions = InstructionDetail::withoutGlobalScope('user')->whereStatus('pending')->get();

        foreach ( $instructions as $instruction )
        {
            $track_time = $instruction->track_time;

            $track_time->addDay();

            $day = $track_time->day;
            $month = $track_time->month;
            $year = $track_time->year;
            
            $schedule->call( function () use ($instruction) {

                $instruction->update([

                    'status' => 'expired'
                ]);

                Mail::to( $instruction->user )->cc(

                    User::whereRole('Director')->get()
                    
                )->queue(
        
                    new ExpiredInstruction($instruction)
                );

            })->cron("* * {$day} {$month} *");
        }
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
