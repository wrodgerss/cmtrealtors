<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\InstructionDetail;

class ExpiredInstruction extends Mailable
{
    use Queueable, SerializesModels;

    public $instruction;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(InstructionDetail $instruction)
    {
        $this->instruction = $instruction;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Expired Instruction')
                    ->view('instruction_details.mail.expired');
    }
}
