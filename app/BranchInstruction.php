<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Builder;

trait BranchInstruction
{
    /* public function newQuery()
    {
        $query = parent::newQuery();

        if (Auth::user()->isAdmin)
        {
            return $query;
        }

        return $query->where(function ($q) {

            // return $q->where('branch_id', Auth::user()->branch_id);

            return $q->where('user_id', Auth::user()->id);
        });
    } */

    /**
     * The "booting" method of the model.
     *
     * @return void
     */
    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('user', function (Builder $builder) {

            if (!Auth::user()->isAdmin)
                $builder->where('user_id', Auth::user()->id);
                
        });
    }
}
