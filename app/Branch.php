<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $fillable = ['name'];

    public function members()
    {
        return $this->hasMany(User::class);
    }

    public function instructions()
    {
        return $this->hasMany(InstructionDetail::class);
    }
}
