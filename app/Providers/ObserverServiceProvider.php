<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\User;
use App\InstructionDetail;
use App\Observers\UserObserver;
use App\Observers\InstructionDetailObserver;

class ObserverServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        User::observe( UserObserver::class );

        InstructionDetail::observe( InstructionDetailObserver::class );
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
