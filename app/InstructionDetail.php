<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Auth;
use Illuminate\Notifications\Notifiable;

class InstructionDetail extends Model
{
    use BranchInstruction;
    use Notifiable;

    protected $dates = ['track_time', 'delivery_date'];

    protected $fillable = [
        'bank', 'bank_branch', 'client_contacts', 'track_time', 'title_no', 'branch_id', 'expenditure', 'user_id', 'payment', 'status'
    ];

    public function branch()
    {
        return $this->belongsTo('App\Branch');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function routeNotificationForMail()
    {
        return $this->user->email;
    }

    public function short_desc()
    {
        return substr($this->title_no, 0, 32);
    }
}
