<?php

namespace App\Policies;

use App\User;
use App\InstructionDetail;
use Illuminate\Auth\Access\HandlesAuthorization;

class InstructionPolicy
{
    use HandlesAuthorization;

    public function before(User $user)
    {
        if($user->isAdmin)
        {
            return true;
        }

        return false;
    }

    /**
     * Determine whether the user can view the instructionDetail.
     *
     * @param  \App\User  $user
     * @param  \App\InstructionDetail  $instructionDetail
     * @return mixed
     */
    public function view(User $user, InstructionDetail $instructionDetail)
    {
        return true;
    }

    /**
     * Determine whether the user can create instructionDetails.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can update the instructionDetail.
     *
     * @param  \App\User  $user
     * @param  \App\InstructionDetail  $instructionDetail
     * @return mixed
     */
    public function update(User $user, InstructionDetail $instructionDetail)
    {
        return false;
    }

    /**
     * Determine whether the user can delete the instructionDetail.
     *
     * @param  \App\User  $user
     * @param  \App\InstructionDetail  $instructionDetail
     * @return mixed
     */
    public function delete(User $user, InstructionDetail $instructionDetail)
    {
        return false;
    }
}
