<?php

namespace App\Http\Controllers;

use App\Branch;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\StoreBranchMember;
use Auth;

class BranchMembersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize(User::class);

        $members = User::where('isAdmin', false)->orderBy('created_at', 'desc')->paginate(10);

        return view('branch_members.index', compact('members'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize(User::class);

        $branches = Branch::all();

        return view('branch_members.create', compact('branches'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreBranchMember  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreBranchMember $request)
    {
        $this->authorize(User::class);

        $branch = Branch::find( $request->branch );

        User::create([
            'branch_id' => $branch->id,
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'role' => $request->role,
            'password' => bcrypt('cmtrealtors@' . $branch->name)
        ]);

        return redirect()->route('members.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $member)
    {
        $this->authorize(User::class);

        $member->delete();

        return redirect(route('members.index'));
    }
}
