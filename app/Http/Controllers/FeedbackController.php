<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Feedback;
use Session;

class FeedbackController extends Controller
{
    public function page()
    {
        return view('feedback');
    }

    public function send_message(Request $request)
    {
        $this->validate($request, [
            'message' => 'required'
        ]);

        Mail::to('briwan.tech@gmail.com')->queue(new Feedback($request->message));

        Session::flash('success', 'Your message has been sent successfully');

        return back();
    }
}
