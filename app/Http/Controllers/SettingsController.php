<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Session;

class SettingsController extends Controller
{
    public function page()
    {
        return view('auth.settings');
    }

    public function change_email(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email|unique:users'
        ]);

        $request->user()->fill([
            'email' => $request->email
        ])->save();

        Session::flash('success_email', 'Your email has been updated');

        return redirect(route('settings'));
    }

    public function change_password(Request $request)
    {
        $this->validate($request, [
            'password' => 'required|min:6'
        ]);

        $request->user()->fill([
            'password' => Hash::make($request->password)
        ])->save();

        Session::flash('success_password', 'Your password has been updated');

        return redirect(route('settings'));
    }
}
