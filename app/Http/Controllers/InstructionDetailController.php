<?php

namespace App\Http\Controllers;

use App\InstructionDetail;
use App\Http\Requests\StoreInstructionDetail;
use Auth;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Notifications\InstructionDelivered;
use App\Branch;
use App\Http\Requests\UpdateInstructionDetail;
use App\Http\Requests\FilterRequest;
use App\User;

class InstructionDetailController extends Controller
{
    public  function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $instructions = InstructionDetail::orderBy('created_at', 'desc')->paginate(9);

        return view('instruction_details.index', compact('instructions'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('instruction_details.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreInstructionDetail $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreInstructionDetail $request)
    {
        $instruction = InstructionDetail::make($request->all());

        if (Auth::user()->isAdmin)
        {
            $instruction->branch_id = User::find( $request->user_id )->branch->id;

            $instruction->user_id = $request->user_id;
        }
        else
        {
            $instruction->branch_id = Auth::user()->branch_id;

            $instruction->user_id = Auth::user()->id;
        }

        $instruction->save();

        return redirect(route('instructions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\InstructionDetail  $instruction
     * @return \Illuminate\Http\Response
     */
    public function show(InstructionDetail $instruction)
    {
        return view('instruction_details.show', compact('instruction'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\InstructionDetail  $instruction
     * @return \Illuminate\Http\Response
     */
    public function edit(InstructionDetail $instruction)
    {
        return view('instruction_details.edit', compact('instruction'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateInstructionDetail  $request
     * @param  \App\InstructionDetail  $instruction
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateInstructionDetail $request, InstructionDetail $instruction)
    {
        $instruction->update($request->all());

        return redirect()->route('instructions.show', $instruction);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\InstructionDetail  $instruction
     * @return \Illuminate\Http\Response
     */
    public function destroy(InstructionDetail $instruction)
    {
        $instruction->delete();

        return redirect(route('instructions.index'));
    }

    public function delivered(InstructionDetail $instruction)
    {
        if ($instruction->status == 'pending')
        {
            $instruction->status = 'completed';
        }

        $instruction->delivery_date = Carbon::now();

        $instruction->save();

        $instruction->notify(new InstructionDelivered($instruction));

        return redirect(route('instructions.show', $instruction->id));
    }

    /**
     * Filter request
     * 
     * @param  \App\Http\Requests\FilterRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function filter(FilterRequest $request)
    {
        if ( $request->get('filter') == 'branch' )
        {
            $instructions = InstructionDetail::where(

                'branch_id',

                Branch::where('name', 'like', '%'. $request->get('query') .'%')->first()->id

            )->get();
        }

        else if ( $request->get('filter') == 'staff' )
        {
            $instructions = InstructionDetail::where(

                'user_id',

                User::where('name', 'like', '%'. $request->get('query') .'%')->first()->id

            )->get();
        }

        else if ( $request->get('filter') == 'status' )
        {
            $instructions = InstructionDetail::where(

                'status', $request->get('query')

            )->get();
        }

        else if ( $request->get('filter') == 'date' )
        {
            $instructions = InstructionDetail::where(

                'created_at', 'like', '%'. $request->get('query') .'%'

            )->get();
        }

        return view('instruction_details.search', compact('instructions'));
    }
}
