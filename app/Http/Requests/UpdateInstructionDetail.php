<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UpdateInstructionDetail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isAdmin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank' => 'required',
            'bank_branch' => 'required',
            'client_contacts' => 'required',
            'track_time' => 'required|date',
            'title_no' => 'required',
            'user_id' => 'required|exists:users,id',
            'expenditure' => 'required',
            'payment' => 'nullable'
        ];
    }
}
