<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreInstructionDetail extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'bank' => 'required',
            'bank_branch' => 'required',
            'client_contacts' => 'required',
            'track_time' => 'required|date',
            'title_no' => 'required',
            'user_id' => 'nullable|exists:users,id',
            'expenditure' => 'required'
        ];
    }
}
