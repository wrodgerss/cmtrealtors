<?php

namespace App\Observers;

use App\User;
use Mail;
use App\Mail\AccountRegistration;

class UserObserver
{
    public function created(User $user)
    {
        Mail::to($user)->queue(
            
            new AccountRegistration($user)
        );
    }
}