<?php

namespace App\Observers;

use App\InstructionDetail;
use App\User;
use Mail;
use App\Mail\NewInstruction;

class InstructionDetailObserver
{
    public function created(InstructionDetail $instruction)
    {
        Mail::to( $instruction->user )->cc(

            User::whereRole('Director')->get()
            
        )->queue(

            new NewInstruction($instruction)
        );
    }
}