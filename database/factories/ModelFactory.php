<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

/** @var \Illuminate\Database\Eloquent\Factory $factory */
$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('mplain5781'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\InstructionDetail::class, function (Faker\Generator $faker) {

    $delivery_date = null;

    switch(random_int(0, 1))
    {
        case 0:
            $status = 'completed';
            $track_time = Carbon\Carbon::now()->subDays(random_int(1, 7));
            $delivery_date = Carbon\Carbon::now()->subDays(random_int(1, 7));
            break;
        case 1:
            $status = 'pending';
            $track_time = random_int(0, 1) == 1 ? Carbon\Carbon::now()->addDays(random_int(1, 7)) : Carbon\Carbon::now()->subDays(random_int(1, 7));
            break;
    }

    return [
        'bank' => $faker->company,
        'bank_branch' => $faker->city,
        'client_contacts' => $faker->phoneNumber,
        'track_time' => $track_time,
        'status' => $status,
        'title_no' => $faker->paragraph,
        'delivery_date' => $delivery_date,
    ];
});

$factory->define(App\Branch::class, function (Faker\Generator $faker) {

    return [
        'name' => $faker->word
    ];
});
