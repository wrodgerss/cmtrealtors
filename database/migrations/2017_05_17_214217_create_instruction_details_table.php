<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('instruction_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('bank');
            $table->string('bank_branch');
            $table->string('client_contacts');
            $table->dateTime('track_time');
            $table->enum('status', ['completed', 'pending', 'expired'])->default('pending');
            $table->longText('title_no');
            $table->integer('branch_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->dateTime('delivery_date')->nullable();
            $table->decimal('expenditure', 17, 2);
            $table->enum('payment', ['paid', 'not paid'])->default('not paid');
            $table->timestamps();

            $table->foreign('branch_id')->references('id')->on('branches')->onDelete('cascade');

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('instruction_details');
    }
}
