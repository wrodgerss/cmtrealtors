<?php

use Illuminate\Database\Seeder;
use App\User;

class SystemAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([

            'name' => 'Brian Kipkoech',
            'email' => 'shadrackbrian@gmail.com',
            'password' => bcrypt('sys-admin@cmtrealtors'),
            'role' => 'Administrator',
            'phone' => '0710533682',
            'isAdmin' => 1
        ]);

        User::create([

            'name' => 'Rodgers Wanyonyi',
            'email' => 'wanyonyirodgers75@gmail.com',
            'password' => bcrypt('sys-admin@cmtrealtors'),
            'role' => 'Administrator',
            'phone' => '0736351761',
            'isAdmin' => 1
        ]);
    }
}
