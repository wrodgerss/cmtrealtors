<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Branch;

class ActualSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        # Branches

        Branch::create(['name' => 'Nairobi']);

        Branch::create(['name' => 'Kisumu']);

        Branch::create(['name' => 'Kisii']);

        Branch::create(['name' => 'Bungoma']);


        # Directors

        User::create([

            'name' => 'Godfrey Omondi',
            'email' => 'gomondi@cmtrealtors.com',
            'password' => bcrypt('sys-admin@cmtrealtors'),
            'role' => 'Director',
            'phone' => '0722737908',
            'isAdmin' => 1
        ]);

        User::create([

            'name' => 'Livingstone Ameso',
            'email' => 'lameso@cmtrealtors.com',
            'password' => bcrypt('sys-admin@cmtrealtors'),
            'role' => 'Director',
            'phone' => '0724538215',
            'isAdmin' => 1
        ]);


        # Nairobi

        $nairobi = Branch::whereName('Nairobi')->first();
            
        $staff = User::make([

            'name' => 'Ibrahim Karomo',
            'email' => 'ikaromo@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Nairobi'),
            'role' => 'Valuer',
            'phone' => '0710870942'
        ]);

        $nairobi->members()->save( $staff );
            
        $staff = User::make([

            'name' => 'Jane Maina',
            'email' => 'accountskenya@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Nairobi'),
            'role' => 'Accountant',
            'phone' => '0711117118'
        ]);

        $nairobi->members()->save( $staff );

        $staff = User::make([

            'name' => 'Dorah Ongaga',
            'email' => 'dongaga@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Nairobi'),
            'role' => 'Marketer',
            'phone' => '0711117118'
        ]);

        $nairobi->members()->save( $staff );

        $staff = User::make([

            'name' => 'Joshua Wanjala',
            'email' => 'jwanjala@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Nairobi'),
            'role' => 'Office Assistant',
            'phone' => '0723987152'
        ]);

        $nairobi->members()->save( $staff );


        # Kisumu

        $kisumu = Branch::whereName('Kisumu')->first();
            
        $staff = User::make([

            'name' => 'Edwin Osundwa',
            'email' => 'eosundwa@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Kisumu'),
            'role' => 'Valuer',
            'phone' => '0705966777'
        ]);

        $kisumu->members()->save( $staff );

        $staff = User::make([

            'name' => 'Steve Oriato',
            'email' => 'soriato@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Kisumu'),
            'role' => 'Administrator',
            'phone' => '0720749580'
        ]);

        $kisumu->members()->save( $staff );

        $staff = User::make([

            'name' => 'Georgina Awuor',
            'email' => 'gawuor@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Kisumu'),
            'role' => 'Office Assistant',
            'phone' => '0797815095'
        ]);

        $kisumu->members()->save( $staff );


        # Kisii

        $kisii = Branch::whereName('Kisii')->first();
            
        $staff = User::make([

            'name' => 'Dancun Njiri',
            'email' => 'dnjiri@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Kisii'),
            'role' => 'Valuer',
            'phone' => '0712348910'
        ]);

        $kisii->members()->save( $staff );

        $staff = User::make([

            'name' => 'Agnes Auma',
            'email' => 'aauma@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Kisii'),
            'role' => 'Office Assistant',
            'phone' => '0725518133'
        ]);

        $kisii->members()->save( $staff );


        # Bungoma

        $bungoma = Branch::whereName('Bungoma')->first();
            
        $staff = User::make([

            'name' => 'Evans Wanoh',
            'email' => 'ewanoh@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Bungoma'),
            'role' => 'Valuer',
            'phone' => '0713032619'
        ]);

        $bungoma->members()->save( $staff );

        $staff = User::make([

            'name' => 'Elizabeth Amunze',
            'email' => 'elizabethamunze3@gmail.com',
            'password' => bcrypt('cmtrealtors@Bungoma'),
            'role' => 'Office Assistant',
            'phone' => '0790289038'
        ]);

        $bungoma->members()->save( $staff );

        $staff = User::make([

            'name' => 'Kennedy Odhiambo',
            'email' => 'kodhiambo@cmtrealtors.com',
            'password' => bcrypt('cmtrealtors@Bungoma'),
            'role' => 'Field Assistant',
            'phone' => '0729016188'
        ]);

        $bungoma->members()->save( $staff );
    }
}
