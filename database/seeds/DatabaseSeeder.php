<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call( DummySeeder::class );

        $this->call( SystemAdminSeeder::class );

        $this->call( ActualSeeder::class );
    }
}
