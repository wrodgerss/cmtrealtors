<?php

use Illuminate\Database\Seeder;
use App\Branch;
use App\User;
use App\InstructionDetail;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(User::class)->create([
            'name' => 'rodgers wanyonyi',
            'email' => 'wanyonyirodgers75@gmail.com',
            'password' => bcrypt('mplain5781'),
            'remember_token' => str_random(10),
            'isAdmin' => 1
        ]);

        factory(Branch::class, 5)->create()->each(function ($branch) {

            $branch->members()->save(factory(User::class)->make());

            factory(InstructionDetail::class, 10)->make()->each(function ($instruction) use ($branch) {

                // $instruction->user_id = $branch->members->random()->id;
                
                $branch->instructions()->save($instruction);
            });
        });
    }
}
