<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    
    return view('home');
});

Route::get('instructions/search', 'InstructionDetailController@filter')->name('instrunctions.search');

Route::resource('instructions', 'InstructionDetailController');

Route::patch('instructions/{instruction}/delivered', 'InstructionDetailController@delivered')->name('instruction.delivered');

Route::get('members', 'BranchMembersController@index')->name('members.index');

Route::get('members/create', 'BranchMembersController@create')->name('members.create');

Route::post('members', 'BranchMembersController@store')->name('members.store');

Route::delete('members/{member}', 'BranchMembersController@destroy')->name('members.destroy');

Route::get('account_registered', 'BranchMembersController@account_registered')->name('account_registered');

Route::post('activate_account', 'BranchMembersController@activate_account')->name('activate_account');

Auth::routes();

Route::get('settings', 'SettingsController@page')->name('settings');

Route::patch('settings/change/email', 'SettingsController@change_email')->name('change_email');

Route::patch('settings/change/password', 'SettingsController@change_password')->name('change_password');

Route::get('feedback', 'FeedbackController@page')->name('feedback');

Route::post('feedback', 'FeedbackController@send_message')->name('send_message');

Route::resource('branches', 'BranchController');
