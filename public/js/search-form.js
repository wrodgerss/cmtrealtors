jQuery(document).ready( function (e) {

    $('#filter').on('change', function(e) {

        if (e.currentTarget.value == 'date')
        {
            $('#query').attr('type', 'date');
        }
        else
        {
            $('#query').attr('type', 'search');
        }
    });
});
